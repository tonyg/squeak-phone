AbstractLauncher subclass: #LinuxIODevStartup
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DevSupport-LinuxIO'!

!LinuxIODevStartup methods!
startUp
	| i |
	FileDirectory setDefaultDirectory: (UnixProcess env at: 'HOME'), '/src/squeak-phone'.
	(UnixProcess env at: 'USER' ifAbsent: [nil])
		ifNotNil: [:u | Utilities authorInitials: u].

	NetNameResolver localHostName caseOf: {
		['zip'] -> [
			PhoneWorldMorph uninstall.
			DisplayScreen fullScreenOn.
		].
	} otherwise: [].

	self update: 'SyndicatedActors' from: (Installer squeaksource project: 'SyndicatedActors').

	i := Installer squeaksource project: 'SqueakPhone'.
	self update: 'LinuxIO' from: i.
	self update: 'SqueakPhone' from: i.

	"If we run the following either here directly, or in the UI
	 process, it appears to lock up the image at 100% CPU usage."
	[RFBServer allowEmptyPasswords: true; stop; start] forkAt: Processor lowestPriority + 1.
!
update: packageName from: anInstaller
	| wc |
	wc := MCWorkingCopy allManagers detect: [:m | m package name = packageName] ifNone: [nil].
	(wc isNil or: [wc modified not]) ifTrue: [anInstaller install: packageName]
!
!

LinuxIODevStartup activate!

Utilities commonRequestStrings: 'DisplayDevice fixDPI "Autodetect"
DisplayDevice fixDPI: 500 "Cellphoneish"
DisplayDevice fixDPI: 256 "Comfortable on my 27-inch monitor"
DisplayDevice fixDPI: 163 "Nominal for a 4k 27-inch monitor"
-
', Utilities commonRequestStrings contents!

SyndicateSnippets stdio1!

2 seconds wait!
