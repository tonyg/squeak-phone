#!/bin/sh
HOST=${1:-pm-rndis}
ssh ${HOST} "stty -F /dev/EG25.AT raw"
socat -v PTY,link=$HOME/src/squeak-phone/pinephone-modem,cfmakeraw EXEC:'ssh -T '"${HOST}"' socat - /dev/EG25.AT'
