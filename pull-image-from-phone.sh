#!/bin/sh
set -eu
HOST=${1:-pm-rndis}
rsync -avu \
      ${HOST}:squeak.changes \
      ${HOST}:squeak.image \
      images/current/.
rsync -avu \
      ${HOST}:squeak-output.log \
      .
