#!/bin/sh

set -eu

HOST=${1:-pm-rndis}

exec ssh -L 5900:localhost:5900 ${HOST}
