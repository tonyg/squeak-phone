#!/bin/sh

# This is needed to let mprotect(2) of /dev/shm/* with PROT_EXEC work on Pinephone
# i.e. to get Cog's JIT to run.
sudo mount -o remount,exec /dev/shm

sudo /usr/lib/squeak/squeak \
     -vm-display-fbdev \
     -fbdev /dev/fb0 \
     -msdev /dev/null \
     -kbdev /dev/null \
     squeak.image 2>&1 | tee -a squeak-output.log
