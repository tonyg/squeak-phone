#!/bin/sh
# 2021-11-10
# revised 2022-01-06 for multiple devices
# revised 2022-10-11 for varying hostnames

set -e
set -u

DEVICEHOSTNAME=${1:-pm-usb0}

eval "$(ssh ${DEVICEHOSTNAME} cat /etc/deviceinfo)"

if [ -z "${deviceinfo_codename}" ]
then
    echo Cannot device which device is connected!
    exit 1
fi

echo Connected device: $deviceinfo_codename

rsnapshot_conf=/tmp/rsnapshot.backups-pm-${deviceinfo_codename}.conf

cat > $rsnapshot_conf <<EOF
config_version	1.2
snapshot_root	/home/tonyg/src/squeak-phone/devices/${deviceinfo_codename}/backups/home-user
no_create_root	0

cmd_cp		/usr/bin/cp
cmd_rm		/usr/bin/rm
cmd_rsync	/usr/bin/rsync
cmd_ssh	/usr/bin/ssh
cmd_logger	/usr/bin/logger

logfile	/tmp/rsnapshot.backups-pm-${deviceinfo_codename}.log
lockfile	/tmp/rsnapshot.backups-pm-${deviceinfo_codename}.pid

retain		adhoc	10000

verbose		2

one_fs	1

sync_first	1

backup	user@${DEVICEHOSTNAME}:/home/user	./
backup	user@${DEVICEHOSTNAME}:/etc/syndicate	./
EOF

rsnapshot -c ${rsnapshot_conf} sync
rsnapshot -c ${rsnapshot_conf} adhoc
