# Actors in the Smalltalk image

## Dataspaces

 - SystemDataspace
 - LinuxInputDataspace
 - TelephonyDataspace
 - WidgetDataspace

## AppRegistryDaemon

 - monitors for interest in open commands in SystemDataspace
     - adds a menu item that, when clicked, issues an open command in same
 - ACTION: should use the shared dataspace instead

## CallManager

 - adds "open dialer" to app menu
     - ACTION: move to shared dataspace
 - monitors telephony dataspace for:
     - active call -> opens call control UI, asserts "call in progress"
     - modem present -> adds to set, removes from set when retracted
 - (re)selects a modem from set of modems whenever no selection exists or current selection not in set
 - unused `hangupAll`, interacts with telephony dataspace
     - BUG: HayesModemCallState needs to respond to <disconnect-call all _>
 - unused `dial:`, interacts with telephony dataspace

## MachineLock

 - SystemDataspace. Assigns locks FIFO to entity carried in <acquire-lock lockid $entity>
 - ACTION: make this machine-wide/shared!!
 - clients:
     - HayesModemActor: <system-device "/dev/...">
     - MockModemActor: <mock-system-device "/dev/...">
     - SamsungGalaxyS7ModemActor: <system-device "/dev/...">

## WidgetDaemon

 - WidgetDataspace. Observers of <window id $w> trigger a new window, with its own dataspace `w`
 - WidgetWindow is a complex beastie

## SoundEffectsDaemon

 - SystemDataspace. Plays sounds in response to continuous-sound and alert-sound assertions.

## AudioProfileDaemon

 - tracks LinuxInputDataspace headset/mic jack events
     - reflects them as presence assertions in LinuxInputDataspace
     - ACTION: move these assertions elsewhere
 - tracks LinuxIOAudioSinkMapping entries to map abstract devices to concrete mixer output names
 - tracks
     - TelephonyDataspace: ringing, call active, speakerphone active
     - SystemDataspace: alert playing
     - LinuxInputDataspace: headset speaker present
 - uses these to determine which subset of {speaker, headset, earpiece} should be enabled
 - uses amixer to turn these on/off in response
 - ACTION: switch to alsaucm and amixer, or be more sophisticated about specifying profiles

## RingToneDaemon

 - monitors TelephonyDataspace for ring, sms, internal-ring conditions and asserts sound effects in response

## BacklightControlDaemon

 - adds brightness control app to menu
 - BrightnessUI does a bunch of sophisticated stuff that might be better done outside the UI?
 - SystemDataspace <blank-screen> --> blanks all framebuffer devices
 - TelephonyDataspace <call-in-progress> --> when speakerphone NOT in use, tracks proximity
     - if proximity (in LinuxInputDataspace) is above a threshold, asserts <disable-touchscreen> and <blank-screen>

## PowerStatusDaemon

 - adds power report app to menu
 - PowerReportUI

## MachineConfiguration

 - selects a MachineVariant and instantiates it during image configuredness

## PinePhoneProximitySensorDaemon

 - LinuxInputDataspace during observation of proximity-above-threshold, tracks proximity
 - polls the device file every 0.1s

## HardwareButtonsDaemon

 - currently just logs switch state changes

## TiledMapDemoApp

 - adds an entry to the apps menu
 - otherwise traditional morphic

## PowerButtonListener

 - LinuxInputDataspace <primary-device-event-type $name KEY> --> listens to it
     - if it's a POWER button, toggles screen blanking and touchscreen disabling

## SmsStore '/home/tonyg/src/squeak-phone/sms-store'

 - TelephonyDataspace sms-delivery and sms-transmission cause new entries in the filesystem-based database
 - adds "open composer" app menu item
 - SmsComposerUI

## MockModem 'mock1'

 - as mock:
     - TelephonyDataspace
         - assert LinuxIOHayesModemPresent
         - on <place-call ...> create a MockModemCall actor
         - on <simulate-incoming-call ...> do the same
         - on <sms-transmission ...> explore the record and acknowledge the continuation
 - HayesModemActor:
     - internal interactions on its own per-modem dataspace
     - messes around with its parent/child linked-actor relationship to have a chance to tear down the modem on shutdown
     - TelephonyDataspace
         - assert LinuxIOHayesModemPresent
         - on <place-call ...>, sends ATD
     - spawns supporting actors:
         - HayesModemCallDriver
             - on unsolicited DSCI events, creates or updates a HayesModemCallState
                 - which interacts via TelephonyDataspace
                     - responds to answer-call, disconnect-call
                     - publishes LinuxIOTelephonyActiveCall
         - HayesModemSmsDriver
             - TelephonyDataspace sms-transmission -> sends it
             - private modem dataspace unsolicited CMTI -> downloads waiting messages
 - SamsungGalaxyS7ModemActor:
     - out of date wrt state of the art, I've been using the PinePhone for dev

# Configuration of the system

All `MachineVariant`s:

 - specify whether or not to debug exceptions in actors
 - start:
    - AppRegistryDaemon
    - WidgetDaemon
    - CallManager
    - SoundEffectsDaemon
    - AudioProfileDaemon
    - RingToneDaemon
    - BacklightControlDaemon
    - HardwareButtonsDaemon
    - PowerStatusDaemon
    - TiledMapDemoApp
    - PowerButtonListener

MachineDesktop:

 - stops LinuxInputDaemon, LinuxInputIndex
 - uninstalls PhoneWorldMorph
 - enables syntax highlighting
 - starts an SmsStore in tonyg's home directory
 - starts either a mock or hayes modem, depending on class configuration
 - configures RingToneDaemon with sounds in tonyg's home directory

MachinePostmarketOS:

 - starts LinuxInputDaemon, LinuxInputIndex
 - installs PhoneWorldMorph
 - restores an audio profile (if there is a sensible one to choose) using `alsactl restore -f -`
 - disables syntax highlighting
 - starts an SmsStore in user's home directory
 - subclasses
    - MachinePinePhone:
         - sets DPI to 270 "We don't seem to be able to discover the correct setting from the hardware yet"
         - starts a hayes modem on '/dev/EG25.AT'
         - starts PinePhoneProximitySensorDaemon
         - configures RingToneDaemon with sounds in user's home directory
         - asserts `LinuxIOAudioSinkMapping`s
             speaker -> Line Out
             headset -> Headphone
             earpiece -> Earpiece
    - MachineQemuAmd64:
         - starts a mock modem on 'mock1'
         - configures RingToneDaemon with sounds in user's home directory
    - MachineSamsungGalaxyS7:
         - supervises SamsungGalaxyS7ModemActor
