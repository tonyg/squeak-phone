#!/bin/sh

# # expects TightVNC Viewer version 1.3.10
# exec xvncviewer -encodings "copyrect tight hextile zlib corre rre raw" localhost:0

# seems to work OK, with scaling even, with ssvnc!
exec ssvncviewer -encodings "copyrect tight hextile zlib corre rre raw" -scale ${SCALE:-0.4} localhost:0
