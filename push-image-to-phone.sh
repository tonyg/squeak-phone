#!/bin/sh
set -eu

HOST=${1:-pm-rndis}
SUDOPASS=${SUDOPASS:-user}

rsync -avu \
      images/current/squeak.changes \
      images/current/squeak.image \
      sounds \
      squeak.pr \
      ${HOST}:.

ssh ${HOST} "echo ${SUDOPASS} | sudo -S cp squeak.pr /etc/syndicate/services/squeak.pr"
