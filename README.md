# SqueakPhone: Running Squeak Smalltalk as the UI for a PostmarketOS-based Cellphone

License: MIT.  
Copyright ©2020–2022 Tony Garnock-Jones.

SqueakPhone comes in two pieces: this is the `git`-based portion, and **there's another part
that lives in [SqueakSource](https://squeaksource.com/SqueakPhone.html)**.

## Setting up your local Unix account

The `LinuxIO` package depends on being able to access `/dev/input/*`, which means on Debian at
least that your user needs to be in the `input` group. This applies not only for running the
image, but for *building* it too, even if you don't plan to develop with it on your local build
host.

## Loading the Smalltalk code into an existing image

You can work with most of the codebase without actually running it on a phone. (In fact, it
also makes sense to transfer images back and forth between desktop VMs and the phone.)

The relevant packages to load into your Squeak image are:

```smalltalk
Installer squeaksource project: 'Preserves'; install: 'Preserves'.
Installer squeaksource project: 'TouchScrolling'; install: 'TouchScrolling'.
Installer squeaksource project: 'TiledMaps'; install: 'TiledMaps'.
Installer squeaksource
	project: 'SyndicatedActors';
	install: 'BTree-Collections';
	install: 'SyndicatedActors'.
Installer squeaksource
	project: 'SqueakPhone';
	install: 'LinuxIO';
	install: 'SqueakPhone'.
```

## Building/installing the system for on-phone use

Follow these steps after [installing the base Synit
system](https://synit.org/book/install.html).

Check out the `squeak-phone` repository:

    git clone https://git.syndicate-lang.org/tonyg/squeak-phone

Install [`squeaker`](https://github.com/tonyg/squeaker) so that the `squeaker` program is on
your `$PATH`.

Run `make dev`. Eventually, a window should open with a transient Squeak image with a fully
loaded set of SqueakPhone packages.

If that worked, try `make images/current`. Then, connect your phone, set up SSH to log in to
it, and run

```shell
./push-image-to-phone.sh PHONEHOSTNAME
```

This will use `rsync` (which has to already have been installed with `apk add rsync` on the
phone) to synchronise

 - the `images/current/` image files
 - the `sounds/` folder
 - `squeak.pr`, the `syndicate-server` configuration responsible for defining and starting the
   Squeak [synit](https://synit.org/) service.

## Notes on working with PostmarketOS

### How to use pmbootstrap

Switching devices:

    pmbootstrap config device <new_device>

e.g.

    pmbootstrap config device pine64-pinephone
    pmbootstrap config device samsung-herolte
    pmbootstrap config device qemu-amd64

Building for Samsung Galaxy S7 (samsung-herolte):

    pmbootstrap install --android-recovery-zip

Building for Pinephone (pine64-pinephone):

    pmbootstrap install --sdcard BLOCKDEV

Building for qemu-amd64:

    pmbootstrap install
    pmbootstrap qemu

### How to install on Samsung Galaxy S7

(See https://wiki.postmarketos.org/wiki/Installation_from_recovery_mode )

Reboot holding power + home + volup to enter the recovery. Choose
Advanced, ADB sideload.

Then run

    pmbootstrap flasher --method adb sideload

### How to use RNDIS to connect via ssh over USB

(See https://wiki.postmarketos.org/wiki/USB_Network )

For pinephone, which *does* get a MAC address for the RNDIS device:

    sudo ip a del 10.42.0.1/32 dev usb0
    sudo ip a add 172.16.42.2/32 dev usb0
    sudo ip r add 172.16.0.0/16 dev usb0
    SSH_AUTH_SOCK= ssh user@172.16.42.1

... or, huh, just use DHCP to get an address from the phone! It turns
out to work.

The equivalent for samsung-herolte is similar but requires inventing a
MAC address and assigning it at the link layer:

    sudo ip link set dev enx000000000000 address 12:12:12:12:12:12
    sudo ip a add 172.16.42.2/32 dev enx000000000000
    sudo ip r add 172.16.0.0/16 dev enx000000000000
    SSH_AUTH_SOCK= ssh user@172.16.42.1

### How to set up WiFi on a running instance

(See https://wiki.postmarketos.org/wiki/WiFi)

When you pick "none" UI, you get no nmcli *or* wpa_supplicant.

Pick "console". Then you get both.

    sudo nmcli device wifi connect <SSID> password <PASSWORD> ifname wlan0
