.PHONY: build-images untag untag-all veryclean dev sudo-dev

SQUEAKERFLAGS=
BUILDFLAGS=

build-images:
	squeaker $(SQUEAKERFLAGS) build $(BUILDFLAGS) -t squeak-phone-base -f Squeakerfile.phone-base.st .
	squeaker $(SQUEAKERFLAGS) build $(BUILDFLAGS) -t squeak-phone -f Squeakerfile.phone.st .
	squeaker $(SQUEAKERFLAGS) build $(BUILDFLAGS) -t squeak-phone-dev -f Squeakerfile.phone-dev.st .

untag:
	squeaker untag squeak-phone squeak-phone-dev

untag-all: untag
	squeaker untag squeak-phone-base

clean: untag
	squeaker gc

veryclean: untag-all
	squeaker gc

dev: build-images
	squeaker run squeak-phone-dev

sudo-dev: build-images
	squeaker run --root squeak-phone-dev

images/%: build-images
	squeaker create squeak-phone "$@"
