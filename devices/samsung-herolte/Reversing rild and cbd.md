https://android.googlesource.com/platform/system/core/+/master/init/README.md

^ Has instructions for attaching strace to services.

Nutshell:

  stop ril-daemon
  setprop ctl.sigstop_on ril-daemon
  start ril-daemon
  ps | grep rild
  strace -tt -xx -T -y -f -s 104857600 -v -o /data/rild.strace.txt -p ...

Hmm, that doesn't work. Sigh.

How to unpack initramfs:
  mkdir q; cd q; zcat ../boot.img-ramdisk.gz | cpio -idv
Repacking:
  find . | cpio --create --format=newc | gzip > ../boot.img-ramdisk.gz

Be careful of [QCDT](https://wiki.postmarketos.org/wiki/QCDT)

/system/xbin/strace -tt -xx -T -y -f -s 104857600 -v -o /data/rild.strace.txt /system/bin/rild



/system/xbin/strace -tt -xx -T -y -s 104857600 -v -fp 3046 2>&1 | tee /data/rild.strace.txt



/system/xbin/strace -tt -xx -T -y -f -s 104857600 -v -o /data/cbd.strace.txt /sbin/cbd -d -tss310 -bm -mm -P platform/155a0000.ufs/by-name/RADIO


/sbin/cbd -d -tss310 -bm -mm -P platform/155a0000.ufs/by-name/RADIO

                                ^ this symlinks to /dev/block/sda8

00000000: 2f73 6269 6e2f 6362 6400 2d64 002d 7473  /sbin/cbd.-d.-ts
00000010: 7333 3130 002d 626d 002d 6d6d 002d 5000  s310.-bm.-mm.-P.
00000020: 706c 6174 666f 726d 2f31 3535 6130 3030  platform/155a000
00000030: 302e 7566 732f 6279 2d6e 616d 652f 5241  0.ufs/by-name/RA
00000040: 4449 4f00                                DIO.


cbd reads sda8's MAIN image 63488 bytes at a time (exactly 62k).
shmem_xmit_boot (and mem_xmit_boot) is what receives each chunk
 - chunks are prefixed with a struct modem_firmware

       struct modem_firmware {
           unsigned long long binary;
           u32 size;
           u32 m_offset;
           u32 b_offset;
           u32 mode;
           u32 len;
       } __packed;

   if mode != 0, we are sending to "IPC region" ("DUMP_MODE")
   if mode == 0, we are sending to "BOOT region" ("BOOT_MODE")

       enum cp_boot_mode {
               CP_BOOT_MODE_NORMAL,
               CP_BOOT_MODE_DUMP,
               CP_BOOT_RE_INIT,
               MAX_CP_BOOT_MODE
       };

   address validity: the following must hold:
    - modem_firmware.size <= total target region size
    - modem_firmware.len <= total target region size
    - modem_firmware.m_offset <= total target region size minus modem_firmware.len

   so this tells us that in modem_firmware:
    - size is the whole size of the target region, the full upload
    - len is the current chunk length
    - m_offset is the offset into the target region

   next, modem_firmware.binary is a userland pointer to the chunk to upload

   VIDEO_SELECT_SOURCE = _IO('o', 25) = _IO('o', 0x19) = IOCTL_MODEM_ON
   VIDEO_CLEAR_BUFFER = _IO('o', 34) = _IO('o', 0x22) = IOCTL_MODEM_BOOT_ON
   VIDEO_SET_ID = _IO('o', 35) = _IO('o', 0x23) = IOCTL_MODEM_BOOT_OFF

   cbd sends 0D 90 00 00, and expects 0D A0 00 00
   cbd sends 00 9F 00 00, and expects 00 AF 00 00
   these are sipc5_link_header structs I think

       /* SIPC5 link-layer header */
       struct __packed sipc5_link_header {
           u8 cfg;
           u8 ch;
           u16 len;
           union {
               struct multi_frame_control ctrl;
               u16 ext_len;
           };
       };

   so: cfg = 0x0d = 0b00001101, ch = 0x90, len = 0 (cfg: start_mask = 1 ??, padding exists ??, control field exists ??)
       cfg = 0x0d = 0b00001101, ch = 0xa0, len = 0 (cfg: start_mask = 1 ??, extension field exists ??)
       cfg = 0x00 = 0b00000000, ch = 0x9f, len = 0
       cfg = 0x00 = 0b00000000, ch = 0xaf, len = 0

  support for this hypothesis: log stmt mentions "std_udl_req_resp",
  which shares a prefix "std_udl_" with definitions in sipc5.h.

  against this hypothesis: the channel numbers are super weird!
  According to modem_v1.h's sipc_ch_id, everything between 32 and 214
  (incl) are reserved, but here we see 144, 160, 159 and 175.

## Using strings on libsec-ril.so

OK so `strings -a libsec-ril.so` yields command type names, but MAYBE out of order.

These are values for cmd_type, for responses:

    INDI    1
    RESP    2
    NOTI    3

These are values presumably for cmd_type for requests:

    EXEC    1
    GET     2
    SET     3
    CFRM    4 ??
    EVENT   5 ??

These are group/main_cmd values - these seem to be in order!:

    PWR_CMD                         1
    CALL_CMD                        2
    CDMA_DATA_CMD
    SMS_CMD                         4
    SEC_CMD                         5
    PB_CMD                          6
        phone book??
    DISP_CMD                        7
    NET_CMD                         8
    SND_CMD                         9
    MISC_CMD                        10
    SVC_CMD                         11
    SS_CMD                          12
    GPRS_CMD                        13
    SAT_CMD                         14
    CFG_CMD                         15
    IMEI_CMD                        16
    GPS_CMD                         17
        sub_cmd=93, noti body: 00
    SAP_CMD                         18
    FACTORY_CMD                     19
    OMADM_CMD
    RFS_CMD
    IMS_CMD
    EMBMS_CMD
    DOMESTIC_CMD
    JPN_CMD
    GEN_CMD                         128
    MAIN_CMD_UNDEFINED

    PWR_PHONE_PWR_UP                1
        noti body: 00
    PWR_PHONE_PWR_OFF               2
    PWR_PHONE_RESET                 3
    PWR_BATT_STATUS                 4
    PWR_BATT_TYPE                   5
    PWR_BATT_COMP                   6
    PWR_PHONE_STATE                 7
        noti body: 02
    PWR_SUB_CMD_UNDEFINED(0x%x)

    CALL_OUTGOING                           1
        body is details of call to place, looks to be a 99-byte FMT packet all told, mostly empty but for destination number
    CALL_INCOMING                           2
        noti body: 00 01 00 01
    CALL_RELEASE                            3
        exec body: empty
    CALL_ANSWER                             4
        exec body: empty
    CALL_STATUS                             5
        noti body: 00 01 00 01 00 00 (dialing)
        noti body: 00 01 00 05 00 00 (connecting)
        noti body: 00 01 00 03 00 00 (connected)
        noti body: 00 01 00 04 00 05 (released)
    CALL_LIST                               6
        req body: empty
        resp body: 01 00 01 00 01 03 00 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37
        resp body: 01 00 01 00 01 04 00 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37
        resp body: 01 00 01 00 01 01 00 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37
        resp body: 00
    CALL_BURST_DTMF
    CALL_CONT_DTMF
    CALL_WAITING
    CALL_LINE_ID
    CALL_SIGNAL
    CALL_VOICE_PRIVACY
    CALL_CALL_TIME_COUNT
    CALL_OTA_PROGRESS
    CALL_DIAG_OUTGOING
    CALL_E911_CB_MODE
    CALL_FLASH_INFO
    CALL_SRVCC
    CALL_HOLD
    CALL_BLOCK_STATUS
    CALL_DATA_CALL_BYTE_COUNTER
    CALL_MODIFY
    CALL_MODIFY_NOTI
    CALL_MODIFY_CONFIRM
    CALL_SUB_CMD_UNDEFINED(0x%x)

    CDMA_DATA_TE2_STATUS
    CDMA_DATA_BYTE_COUNTER
    CDMA_DATA_INCOMING_CALL_TYPE
    CDMA_DATA_TE2_DIALING_INFO
    CDMA_DATA_TE2_DATA_RATE_INFO
    CDMA_DATA_PACKET_DATA_CALL_CFG
    CDMA_DATA_DS_BAUD_RATE
    CDMA_DATA_MOBILE_IP_NAI
    CDMA_DATA_CURRENT_NAI_INDEX
    CDMA_DATA_DORMANT_CONFIG
    CDMA_DATA_MIP_NAI_CHANGED
    CDMA_DATA_SIGNEDIN_STATE
    CDMA_DATA_RESTORE_NAI
    CDMA_DATA_MIP_CONNECT_STATUS
    CDMA_DATA_DORMANT_MODE_STATUS
    CDMA_DATA_R_SCH_CONFIG
    CDMA_DATA_HDR_SESSION_CLEAR
    CDMA_DATA_SESSION_CLOSE_TIMER_EXPIRED
    CDMA_DATA_KEEPALIVETIMER_VALUE
    CDMA_DATA_DDTMMODE_CONFIG
    CDMA_DATA_ROAM_GUARD
    CDMA_DATA_MODEM_NAI
    CDMA_DATA_KOREA_MODE
    CDMA_DATA_DATA_SERVICE_TYPE
    CDMA_DATA_FORCE_REV_A_MODE
    CDMA_DATA_CUSTOM_CONFIG_MODE
    CDMA_DATA_NAI_SETTING_MODE
    CDMA_DATA_PIN_CTRL
    CDMA_DATA_RAW_DATA_MODE
    CDMA_DATA_DUN_MODE
    CDMA_DATA_TE2_CALL_STATUS
    CDMA_DATA_IP_CONFIGURATION
    CDMA_DATA_CALL_STATUS
    CDMA DATA_SUB_CMD_UNDEFINED(0x%x)

    SMS_SEND_MSG                    1
    SMS_INCOMING_MSG                2
        noti body: 2020-09-09 12:40:45.157159 ipc0 (0035) STATE_ONLINE Packet(len=53, msg_seq=62, ack_seq=0, main_cmd=4, sub_cmd=2, cmd_type=3) 0201ffff012807911356131313f3040b911356974843f70000029090210444800ec67219644e83cc6f90b9de0e01 b'\x02\x01\xff\xff\x01(\x07\x91\x13V\x13\x13\x13\xf3\x04\x0b\x91\x13V\x97HC\xf7\x00\x00\x02\x90\x90!\x04D\x80\x0e\xc6r\x19dN\x83\xcco\x90\xb9\xde\x0e\x01'
            - that's "Fee fi fo fum!" from my main number
              02 01 ff ff 01 28
              07 91 13 56 13 13 13 f3           the SMSC number
              04 0b 91 13 56 97 48 43 f7
              00 00 02 90 90 21 04 44 80
              0e c6 72 19 64 4e 83 cc 6f 90 b9 de 0e 01
               = 11000110 01110010 00011001 01100100 01001110 10000011 11001100 01101111 10010000 10111001 11011110 00001110 00000001
               --> 1000110 F
                   1100101 e
                   1100101 e
                   0100000 SP
                   1100110 f
                   1101001 i
                   0100000 SP
                   1100110 f
                   1101111 o
                   0100000 SP
                   1100110 f
                   1110101 u
                   1101101 m
                   0100001 !
    SMS_READ_MSG                    3
    SMS_SAVE_MSG                    4
    SMS_DEL_MSG                     5
    SMS_DELIVER_REPORT              6
    SMS_DEVICE_READY                7
        noti body: 02
    SMS_SEL_MEM                     8
    SMS_STORED_MSG_COUNT            9
    SMS_SVC_CENTER_ADDR             10
    SMS_SVC_OPTION                  11
    SMS_MEM_STATUS                  12
    SMS_CBS_MSG                     13
    SMS_CBS_CFG                     14
        set body: 01 80 03 11 00 11 01 11 02 [logcat_ALL.txt]
        set body: 01 80 04 11 00 11 01 11 02 11 04 [logcat_ALL.txt]
        set body: 01 80 06 11 00 11 01 11 02 11 04 11 13 11 14 [logcat_ALL.txt]
        set body: 01 80 0C 11 00 11 01 11 02 11 04 11 13 11 14 11 15 11 16 11 17 11 18 11 19 11 1A [logcat_ALL.txt]
        set body: 01 80 0D 11 00 11 01 11 02 11 04 11 13 11 14 11 15 11 16 11 17 11 18 11 19 11 1A 11 1B [logcat_ALL.txt]
    SMS_STORED_MSG_STATUS           15
    SMS_PARAM_COUNT                 16
    SMS_PARAM                       17
    SMS_STATUS
    SMS_SUB_CMD_UNDEFINED(0x%x)

        sub_cmd=12, noti body: 0A 98 13 80 40 02 30 16 03 31 F7
    SEC_PIN_STATUS                  1
        sub_cmd=1, noti body: 00 00
        sub_cmd=1, noti body: 82 00
        sub_cmd=1, noti body: 83 00
    SEC_PHONE_LOCK                  2
    SEC_CHANGE_LOCKING_PW           3
    SEC_SIM_LANG                    4
    SEC_RSIM_ACCESS                 5
    SEC_GSIM_ACCESS                 6
    SEC_SIM_ICC_TYPE                7
        sub_cmd=7, noti body: 02
    SEC_LOCK_INFOMATION             8
    SEC_IMS_AUTH                    9
    SEC_RUIM_CONFIG
    IPC_SEC_SIMAPPS_INFO
    SEC_SUB_CMD_UNDEFINED(0x%x)

    PB_ACCESS                           1
    PB_STORAGE                          2
    PB_STORAGE_LIST                     3
    PB_ENTRY_INFO                       4
    PB_3GPB_CAPA                        5 ??
        noti body: 010701fa001200030002fa00280003000396002800000004640028000000050a001200000006fa000500030007fa0002000300
    PB_SUB_CMD_UNDEFINED(0x%x)

    DISP_ICON_INFO                      1
        noti body: 01 01 00 00 00 00 FF FF FF FF
        noti body: 01 02 00 00 63 67 09 A0 00 02
    DISP_HOMEZONE_INFO
    DISP_PHONE_FATAL_INFO
    DISP_EXT_ROAM_INFO
    DISP_USER_INDICATION
    DISP_RSSI_INFO                      6
        noti body: 63 07 63 63 00 FF FF FF FF 10 FF FF
        noti body: 65 06 63 63 00 FF FF FF FF 11 FF FF
        noti body: 6B 63 63 04 6B 0A A0 00 09 FF FC FF
    DISP_SUB_CMD_UNDEFINED(0x%x)

    NET_PREF_PLMN                       1
    NET_PLMN_SEL                        2
        get body: empty
        resp body: 02
        resp body: 05
    NET_SERVING_NETWORK                 3
        noti body: 02 02 04 32 30 34 30 38 23 50 19 00 F0 21 A7 00 21 FD FF 00 00 FF 00 00 FF FF
        get body: empty
        noti body: 05 02 21 32 30 34 30 38 23 00 00 00 0D 53 7B 00 21 FD FF 00 00 FF 00 00 7B 01
        resp body: 05 02 21 32 30 34 30 38 23 00 00 00 0D 53 7B 00 21 FD FF 00 00 FF 00 00 7B 01
    NET_PLMN_LIST                       4
    NET_REGIST                          5
        noti body: 04 02 02 00 50 19 F0 21 A7 00 00 21 FD 02 02 00 FF FF 00
        noti body: 04 03 02 00 50 19 F0 21 A7 00 00 21 FD 02 02 00 FF FF 00
        get body: FF 03
        get body: FF 02
        noti body: 21 01 02 00 00 00 0D 53 7B 00 00 21 FD 02 02 00 7B 01 00
        resp body: 21 03 02 00 00 00 0D 53 7B 00 00 21 FD 02 02 00 7B 01 00
        resp body: 21 02 02 00 00 00 0D 53 7B 00 00 21 FD 02 02 00 7B 01 00
    NET_SUBSCRIBER_NUM                  6
    NET_BAND_SEL                        7
    NET_SERVICE_DOMAIN_CONFIG           8
    NET_POWERON_ATTACH                  9
    NET_MODE_SEL                        10
    NET_ACQ_ORDER                       11
    NET_IDENTITY                        12
    NET_PREFERRED_NETWORK_INFO          13
    NET_HYBRID_MODE
    NET_AVOID_SYS
    NET_HANDOVER
    NET_DUAL_STANDBY_PREF
    NET_VOWIFI_HO_THRESHOLD
    NET_EPDG_HO_THRESHOLD
    NET_SUB_CMD_UNDEFINED(0x%x)

    SND_SPKR_VOLUME_CTRL                    1
        set body: 01 05
        set body: 01 04
    SND_MIC_MUTE_CTRL                       2
    SND_AUDIO_PATH_CTRL                     3
        set body: 01 00
        set body: 06 62 [logcat_ALL.txt]
        set body: 06 18 [logcat_ALL.txt]
        set body: 06 68 [logcat_ALL.txt]
        set body: 06 D2 [logcat_ALL.txt] and more variations
    SND_AUDIO_SOURCE_CTRL                   4
    SND_LOOPBACK_CTRL                       5
    SND_VOICE_RECORDING_CTRL                6
    SND_VIDEO_CALL_CTRL                     7
    SND_RINGBACK_TONE_CTRL                  8
        noti body: 00
    SND_CLOCK_CTRL                          9
        exec body: 00
        noti body: 05
        noti body: 00
    SND_WB_AMR_RPT                          10
        noti body: 01
    SND_TWO_MIC_SOL_CTRL                    11
        set body: 00 01

    SND_DHA_SOL_CTRL
    SND_AUDIO_MODE_CTRL
    SND_CLOCK_MODE_CTRL
    SND_KEY_TONE
    SND_NOTI_TONE
    SND_LED_CTRL
    SND_VIB_CTRL
    SND_MIC_GAIN_CTRL
    SND_SPKR_PHONE_CTRL
    SND_HFK_AUDIO_STARTSTOP
    SND_VOICECALL_RECORD_REPORT
    SND_USER_SND_CONFIG
    SND_GAIN_CTRL
    SND_QUIET_MODE_CTRL
    SND_DYVE_MODE_CTRL
    SND_SUB_CMD_UNDEFINED(0x%x)

    MISC_ME_VERSION                 1
    MISC_ME_IMSI                    2
    MISC_ME_SN                      3
    MISC_KEY_EVENT_PROCESS
    MISC_TIME_INFO                  5
        noti body: 02 01 14 09 08 0B 19 1D 08 01 02 01 32 30 34 30 38 23
                       2020-09-08 11:25:29  8-quarter-hours?
                                                       20408# -- the network??
        2020-09-09 11:47:58.411493 noti body: 02 01 14 09 09 09 2f 3a 08 01 03 01 32 30 34 30 38 23
                                                  2020-09-09 09:47:58  8-quarter-hours?
                                                                                  20408# -- the network??

    MISC_NAM_INFO
    MISC_VCALL_CHANNEL_ID
    MISC_PHONE_DEBUG
    MISC_FUS
    MISC_PDA_BOOT_COMPLETE
    MISC_FACTORY_RESET_COMPLETE
    MISC_SCREEN_STATUS
    MISC_GRIP_SENSOR_STATUS
    MISC_OMADM_CDMA_NAM_INFO
    MISC_OMADM_PRL_WRITE
    MISC_LTE_TIMER
    MISC_DUALSTANDBY_CALL_STATUS
    MISC_SUB_CMD_UNDEFINED(0x%x)

    SVC_ENTER                       1
    SVC_END                         2
    SVC_PRO_KEYCODE                 3
    SVC_SCREEN_CFG                  4
    SVC_DISPLAY_SCREEN              5
    SVC_CHANGE_SVC_MODE             6
    SVC_DEVICE_TEST                 7
    SVC_DEBUG_DUMP_MESSAGE          8
    SVC_DEBUG_STRING_MESSAGE        9
    SVC_CALL_DROP_LOG_INFO
    SVC_LTE_SCAN_FILE
    SVC_MENU_INFO
    SVC_SUB_CMD_UNDEFINED(0x%x)

    SS_WAITING                      1
    SS_CLI                          2
    SS_BARRING                      3
    SS_BARRING_PW                   4
    SS_FORWARDING                   5
    SS_INFO                         6
        is this info about an incoming call? looks like it?
        noti body: 0A 00 00 00 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    SS_MANAGE_CALL                  7
    SS_USSD                         8
    SS_AOC                          9
    SS_RELEASE_COMPLETE             10
    SS_SUB_CMD_UNDEFINED(0x%x)

    GPRS_DEFINE_PDP_CONTEXT
    GPRS_QOS
    GPRS_PS                         3
        set body: 00 00 04 [logcat_ALL.txt]
        noti body: 00 00 05 [logcat_ALL.txt]
    GPRS_PDP_CONTEXT
    GPRS_ENTER_DATA
    GPRS_SHOW_PDP_ADDR
    GPRS_MS_CLASS
    GPRS_3G_QUAL_SRVC_PROFILE
    GPRS_IP_CONFIGURATION
    GPRS_DEFINE_SEC_PDP_CONTEXT
    GPRS_TFT
    GPRS_HSDPA_STATUS                       12
        noti body: 00
    GPRS_CURRENT_SESSION_DATA_COUNTER
    GPRS_DATA_DORMANT
    GPRS_PIN_CTRL                           15 ??
        noti body: 06 00
    GPRS_CALL_STATUS
    GPRS_PORT_LIST
    GPRS_LTE_QOS
    GPRS_NWK_INIT_DISCONNECT
    GPRS_FD_INFORMATION
    GPRS_TRAFFIC_CHANNEL_STATUS             28
        noti body: 01 [logcat_ALL.txt]
    GPRS_MOBILE_DATA_STATUS

    GPRS_LTE_QOS_PROFILE
    GPRS_NW_INITIATED_PDN_DISCONNECT
    GPRS_LTE_ATTACH_APN_INFO                20
        set body: 01 01 02 61 69 72 74 65 6C 67 70 72 73 2E 63 6F 6D 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 [logcat_ALL.txt]
    GPRS_EPDG_HANDOVER
    GPRS_EPDG_STATUS
    GPRS_OPERATOR_RESERVED_PCO
    GPRS_LTE_CA_STATUS                      25
        noti body: 00 0A
        noti body: 01 1E
    GPRS_BACKOFF_TIMER
    GPRS_SET_APN_INFO
    GPRS_IMS_TEST_MODE
    GPRS_SET_ALWAYS_ON_PDN

    SAT_PROFILE_DOWNLOAD                1
    SAT_ENVELOPE_CMD                    2
    SAT_PROACTIVE_CMD                   3
    SAT_TERMINATE_USAT_SESSION          4
    SAT_EVENT_DOWNLOAD                  5
    SAT_PROVIDE_LOCAL_INFO              6
    SAT_POLLING                         7
    SAT_REFRESH                         8
    SAT_SETUP_EVENT_LIST                9
    SAT_CALL_CONTROL_RESULT             10
        body: have seen 00 00, so far
    SAT_IMAGE_CLUT                      11
    SAT_SETUP_CALL_PROCESSING           12
    SAT_SIM_INITIATE_MESSAGE
    SAT_SUB_CMD_UNDEFINED(0x%x)

    CFG_DEFAULT_CONFIG
    CFG_EXTERNAL_DEVICE
    CFG_MAC_ADDRESS
    CFG_CONFIGURATION_ITEM
    CFG_TTY
    CFG_HSDPA_TMP_SETTING
    CFG_HSDPA_PERM_SETTING
    CFG_SIO_MODE
    CFG_AKEY_VERIFY
    CFG_MSL_INFO
    CFG_USER_LOCK_CODE
    CFG_USB_PATH
    CFG_CURRENT_SVC_CARRIER
    CFG_RADIO_CONFIG
    CFG_VOCODER_OPTION
    CFG_TEST_SYS
    CFG_RECONDITIONED_DATE
    CFG_PROTOCOL_REVISION
    CFG_SLOT_MODE
    CFG_ACTIVATION_DATE
    CFG_CURRENT_UATI
    CFG_QUICK_PAGING
    CFG_LMSC_INFO
    CFG_TAS_INFO
    CFG_AUTH_INFO
    CFG_HIDDEN_MENU_ACCESS
    CFG_UTS_SMS_SEND
    CFG_UTS_SMS_COUNT
    CFG_UTS_SMS_MSG
    CFG_SCM_INFO
    CFG_SCI_INFO
    CFG_ACCOLC_INFO
    CFG_MOBTERM_INFO
    CFG_1X_EVDO_DIVERSITY_CONFIG
    CFG_DEVICE_CONFIGURATION
    CFG_USER_LOCK_CODE_STATUS
    CFG_UTS_SMS_GET_UNREAD_MSG_STATUS
    CFG_MOBILEAP_STATUS
    CFG_ADVANCED_INFO
    CFG_WDC
    CFG_GET_OPERATOR
    CFG_GET_VOICEMAIL_NUM
    CFG_GET_WALLPAPER_STATUS
    CFG_GET_PWRONOFF_IMG_STATUS
    CFG_SAFE_MODE
    CFG_SEC_VALIDATE
    CFG_SAR_DEVICE
    CFG_MMS_PARAM
    CFG_HIDDEN_PROGRAM
    CFG_SUB_CMD_UNDEFINED(0x%x)

    IMEI_START
    IMEI_CHECK_DEVICE_INFO
    IMEI_PRE_CONFIG
    IMEI_WRITE_ITEM
    IMEI_REBOOT
    IMEI_VERIFY_FACTORY_RESET
    IMEI_COMPARE_ITEM
    IMEI_MASS_STORAGE_INFO
    IMEI_SUB_CMD_UNDEFINED(0x%x)

    GPS_OPEN
    GPS_CLOSE
    GPS_START
    GPS_DEVICE_STATE
    GPS_OPTION
    GPS_TTFF
    GPS_LOCK_MODE
    GPS_SECURITY_UPDATE
    GPS_SSD
    GPS_SECURITY_UPDATE_RATE
    GPS_FIX_REQ
    GPS_POSITION_RESULT
    GPS_EXT_POSITION_RESULT
    GPS_EXT_STATUS_INFO
    GPS_PD_CMD_CB
    GPS_DLOAD_STATUS
    GPS_END_SESSION
    GPS_FAILURE_INFO
    GPS_HW_STATE
    GPS_SECURITY_ENABLE
    GPS_SECURITY_READ
    GPS_SECURITY_WRITE
    GPS_ENCRYPT_READ
    GPS_REF_LOCATION
    GPS_PGPS_TIME_INFO
    GPS_PGPS_EXT_EPH
    GPS_PGPS_BROADCAST_EPH
    GPS_SENSOR_INFO
    GPS_RXN_REF_LOCATION
    GPS_RXN_UPDATE_CELLID
    GPS_READY_NOTI
    GPS_UTS_GET_POSITION
    GPS_XTRA_SET_TIME_INFO
    GPS_XTRA_SET_DATA
    GPS_XTRA_CLIENT_INIT_DOWNLOAD
    GPS_XTRA_QUERY_DATA_VALIDITY
    GPS_XTRA_SET_AUTO_DOWNLOAD
    GPS_XTRA_SET_XTRA_ENABLE
    GPS_XTRA_DOWNLOAD
    GPS_XTRA_VALIDITY_STATUS
    GPS_XTRA_TIME_EVENT
    GPS_XTRA_DATA_INJECTION_STATUS
    GPS_XTRA_USE_SNTP
    GPS_CP_POWER_ON
    GPS_CP_FREQ_AIDING
    GPS_CP_PRECISE_TIME_AIDING
    GPS_CP_PSEUDORANGE_MSMT
    GPS_CP_SESSION_CANCELLATION
    GPS_AGPS_PDP_CONNECTION
    GPS_AGPS_DNS_QUERY
    GPS_AGPS_SSL
    GPS_AGPS_MODE
    GPS_VERIFICATION
    GPS_DISPLAY_SUPLFLOW
    GPS_SUB_CMD_UNDEFINED(0x%x)

    SAP_CONNECT
    SAP_STATUS
    SAP_TRANSFER_ATR
    SAP_TRANSFER_APDU
    SAP_TRANSPORT_PROTOCOL
    SAP_SIM_POWER
    SAP_TRANSFER_CARD_READER_STATUS
    SAP_SUB_CMD_UNDEFINED(0x%x)

    FACTORY_DEVICE_TEST
    FACTORY_OMISSION_AVOIDANCE_TEST
    FACTORY_DFT_TEST
    FACTORY_MISCELLANEOUS_TEST
    FACTORY_BYPASS_TEST                 5 ??
    FACTORY_SLATE_TEST
    FACTORY_FRAME_BUFFER_TEST
    FACTORY_DIAG_PST_UTS
    FACTORY_SEMI_FINAL_TEST
    FACTORY_SUB_CMD_UNDEFINED(0x%x)

    OMADM_PRL_SIZE
    OMADM_MODEL_NAME
    OMADM_OEM_NAME
    OMADM_SW_VER
    OMADM_IS683_DATA
    OMADM_PRL_READ
    OMADM_PRL_WRITE
    OMADM_PUZL_DATA
    OMADM_ROOTCERT_READ
    OMADM_ROOTCERT_WRITE
    OMADM_MMC_OBJECT
    OMADM_MIP_NAI_OBJECT
    OMADM_CURRENT_NAI_INDEX
    OMADM_MIP_AUTH_ALGO
    OMADM_NAM_INFO
    OMADM_START_CIDC
    OMADM_START_CIFUMO
    OMADM_START_CIPRL
    OMADM_START_HFA
    OMADM_START_REG_HFA
    OMADM_SETUP_SESSION
    OMADM_SERVER_START_SESSION
    OMADM_CLIENT_START_SESSION
    OMADM_SEND_DATA
    OMADM_ENABLE_HFA
    OMADM_SUB_CMD_UNDEFINED(0x%x)

    EMBMS_SERVICE_MSG
    EMBMS_SESSION_MSG
    EMBMS_BSSI_MSG
    EMBMS_COVERAGE_MSG
    EMBMS_SESSION_LIST_MSG
    EMBMS_SIGNAL_STRENGTH_MSG
    EMBMS_SIB16_NETWORK_TIME_MSG
    EMBMS_SAI_LIST_MSG
    EMBMS_GLOBAL_CELL_ID_MSG
    EMBMS_SUB_CMD_UNDEFINED(0x%x)

    DOMESTIC_CHANNEL_SETTING
    DOMESTIC_LTE_RRC_SETTING
    DOMESTIC_DELETE_STORED_CELL_LIST
    DOMESTIC_ACTIVATION_DATE
    DOMESTIC_SECURITY_MODE
    DOMESTIC_HSDPA_ON_OFF
    DOMESTIC_LAST_CALL
    DOMESTIC_LTE_THROUGHPUT_TEST_MENU
    DOMESTIC_CARD_TYPE
    DOMESTIC_BAND_SEL
    DOMESTIC_GCF_TEST_MODE
    DOMESTIC_OTA_REG_MODE
    DOMESTIC_NET_REG_STATUS_UI
    DOMESTIC_NSRI_PROCESS
    DOMESTIC_NSRI_TOAST_CMD
    DOMESTIC_DISABLE_LTE_B7
    DOMESTIC_MOBILE_QUALITY_INFO
    DOMESTIC_ANDROID_ENTER_DIALER
    DOMESTIC_KEEP_LTE_ICON_CSFB_SETTING
    DOMESTIC_VOICE_CALL_STATUS
    DOMESTIC_SYSTEM_INFO_FOR_LTE
    DOMESTIC_SYSTEM_INFO_FOR_WIPI
    DOMESTIC_NSRI_SECURE_CALL_MODE
    DOMESTIC_IMSI_CHANGED_INFO
    DOMESTIC_NETWORK_INFO_NOTI
    DOMESTIC_PS_BARRING_FOR_VOLTE
    DOMESTIC_KT_HD_VOICE_STATUS
    DOMESTIC_UKNIGHT_INFO
    DOMESTIC_LTE_WIDEBAND_INFO
    IPC_DOMESTIC_LTE_ROAMING_STATUS
    DOMESTIC_PROTOCOL_FEATURE
    DOMESTIC_NSRI_ENCRYPT_SMS
    DOMESTIC_NSRI_DECRYPT_SMS
    DOMESTIC_NSRI_DECRYPTTX_SMS
    DOMESTIC_NSRI_CHECK_SUSIM
    DOMESTIC_NSRI_REQUEST_PROC
    DOMESTIC_SUB_CMD_UNDEFINED(0x%x)

    GEN_PHONE_RES                               1
        body is main/sub/type of request, plus response bytes -- usually 00 80 for "OK" I suppose
          - these are a `short`, so 0x8000 for OK I guess
    GEN_SUB_CMD_UNDEFINED(0x%x)

    IPC_MMB_NVINFO
    IPC_DATA_SETTINGINFO
    IPC_JPN_AC_BARRING_FOR_VOLTE
    IPC_JAPAN_KDDI_SIMBLOB

    JPN_SUB_CMD_UNDEFINED(0x%x)

    MAIN_AND_SUB_UNDEFINED(0x%x,0x%x)

Oh no! There's more!

    MMS_PROVISION_CMD
    MODEM_TEST_CMD
    PCSC_CMD
    QMI_HIDDENMENU_CMD
    SMARTAS_CMD
    PROSE_CMD
    MCPTT_CMD
    QMIIMS_CMD
    IIL_CMD

    CALL_LINE_CTRL
    CALL_ALERT_CONTROL
    CALL_ECCLIST
    CALL_ECC_STATUS
    CALL_VOICE_RADIO_BEARER_HANDOVER

    CDMA_DATA_SERVICE_CFG
    CDMA_DATA_CALL_ESTABLISH
    CDMA_DATA_SIGN_IN_STATE
    DATA_CALL_STATUS
    CDMA_DATA_WORKING_MODE
    CDMA_DATA_EVDO_REVISION_CONFIG
    CDMA_DATA_SIP_PARAMETER
    CDMA_DATA_CALL_STATUS_IPBASED

    SMS_CBS_CBMI_CFG
    SMS_CBS_ACTIVATION
    IPC_SMS_RP_SMMA

    SEC_ATR_INFO
    SEC_SIM_ICCID
    SEC_SIM_POWER
    SEC_IMS_SUPP_AUTH_TYPES

    NET_CELL_INFO                       17
        get body: empty
        resp body: 00 00 00 01 01 32 30 34 30 38 23 50 19 F0 21 A7 00 7A 01 C3 0B 0F 00
        resp body: 00 00 01 00 01 32 30 34 30 38 23 0D 53 7B 00 7B 01 00 00 21 FD 00 19 00 00 20 6B 0A FC FF 09 0E 00 00 00
    NET_ECC_RAT
    NET_CSG_SEARCH
    NET_PREFERRED_ROAMING_PLMN_LIST
    NET_DOMAIN_SPECIFIC_RESTRICTED
    NET_ACB_INFO
    NET_SSAC_INFO
    NET_LTE_BAND_PRIORITY
    NET_LTE_ROAMING
    NET_CA
    NET_AUTONOMOUS_GAP
    NET_DISABLE_2G
    NET_NAS_TIMER

    MISC_ALARM_INFO
    MISC_PUBLIC_MODE
    MISC_RESERVED
    MISC_DEVICE_POSITION
    MISC_VTCALL_CONNECTION_STATUS
    MISC_PREPAY_MODE
    MISC_TBSR_CDMA
    MISC_MODEM_INTERFACE_MODE
    MISC_MODEM_UART_MODE
    MISC_T_MPSR_VALUE
    MISC_ENS_STATE
    MISC_1XADV_INFO
    MISC_SMS_FORMAT
    MISC_SMS_OVER_IP_INFO
    MISC_HOME_DOMAIN_NAME
    MISC_1X_SVC_DELAY_TIMER
    MISC_TSIP_TIMER
    MISC_UART_AUTO
    MISC_IMS_TESTMODE
    MISC_IMS_SIP_PORT
    MISC_IMS_FQDN_CSCF
    MISC_IPC_LOOPBACK
    MISC_PA_THERMISTER
    MISC_LOGGING_TIME_INFO
    MISC_SILENT_LOGGING_CONTROL
    MISC_GPIODVS_DATA
    MISC_AT_CMD_FWD
    MISC_SIMLOCK_SHARED_KEY
    MISC_SIMLOCK_BLOB
    MISC_DEVICE_CAPA
    MISC_CA_PROPERTY
    MISC_T3402_TIMER
    MISC_POA_DELETE_GUTI
    MISC_BIP_INFO
    MISC_CP_POSITION
    MISC_GRIP_SENSOR_INFO
    MISC_AP_GPS_POSITION
    MISC_LCD_MIPI_CONTROL
    MISC_CLM_TT_CMD
    MISC_ECHOLOCATE_MENU_STATUS
    MISC_SSDS_ONEHW_SIM_SLOT_COUNT
    MISC_CP_SPD_STATUS
    MISC_MANUFACTURE_SALES_CODE
    MISC_SHARED_MEM_CMD_SAVE_FULL_MEMORY
    MISC_SEND_CP_FEATURE
    IPC_MISC_CP_NW_DATA

    SVC_BIG_DATA_INFO               13
        noti body: 00 DB 00 7B 22 4A 56 45 52 22 3A 22 53 50 4C 31 22 2C 22 48 57 5F 56 22 3A 22 4D 50 5F 30 2E 37 30 30 22 2C 22 43 74 79 70 22 3A 22 31 22 2C 22 50 4C 4D 4E 22 3A 22 32 30 34 30 38 23 22 2C 22 41 43 54 5F 22 3A 22 32 22 2C 22 52 41 43 5F 22 3A 22 30 22 2C 22 4C 41 43 5F 22 3A 22 31 39 35 30 22 2C 22 54 41 43 5F 22 3A 22 30 30 30 30 22 2C 22 43 5F 49 44 22 3A 22 41 37 32 31 46 30 22 2C 22 50 68 49 44 22 3A 22 33 37 38 22 2C 22 44 4C 43 68 22 3A 22 33 30 31 31 22 2C 22 52 67 53 74 22 3A 22 32 22 2C 22 52 6A 43 75 22 3A 22 30 22 2C 22 47 52 49 50 22 3A 22 32 22 2C 22 45 41 52 4A 22 3A 22 30 22 2C 22 41 55 53 54 22 3A 22 34 22 2C 22 54 78 41 53 22 3A 22 33 22 7D
        noti body: 09a1007b224a564552223a2253504c31222c22504c4d4e223a22323034303823222c224143545f223a2234222c225241435f223a2230222c224c41435f223a2230303030222c225441435f223a2230303030222c22435f4944223a2230222c2250684944223a2230222c22444c4368223a2236343030222c2252675374223a2232222c2254595045223a2232222c22454d4d43223a2230222c2245534d43223a2230227d b'\t\xa1\x00{"JVER":"SPL1","PLMN":"20408#","ACT_":"4","RAC_":"0","LAC_":"0000","TAC_":"0000","C_ID":"0","PhID":"0","DLCh":"6400","RgSt":"2","TYPE":"2","EMMC":"0","ESMC":"0"}' 
    SVC_AUTOMATION_INFO
    SVC_FAKE_CELL_INFO

    SS_UUS
    SS_IC_BARRING
    SS_EXTRAS
    SS_TRANSFER_CALL

    CFG_UART_MODEM_PATH
    CFG_DEBUG_MSG_LEVEL
    CFG_UTS_HIDDEN_MENU_ACCESS
    CFG_SIM_LOCK_INFO
    CFG_SIM_UICCID
    CFG_SSD_DATA
    CFG_SAR_CONTROL

    IMEI_MASS_STORAGE_FILE_NUMBER
    IMEI_UPDATE_ITEM
    IMEI_VERIFY_COMPARE_STATUS
    IPC_IMEI_CERT_STATUS

    GPS_INIT
    GPS_DEINIT
    GPS_STOP_SESSION
    GPS_POSITION_DATA
    GPS_EXT_MEASURMENT
    GPS_PARAMETERS
    GPS_DATA_CONNECTION
    GPS_DNS_LOOKUP
    GPS_PD_EVENT
    GPS_XTRA_INIT
    GPS_XTRA_DEINIT
    GPS_XTRA_ENABLE
    GPS_XTRA_TIME
    GPS_XTRA_DATA
    GPS_EXT_RADIO_SIG
    GPS_CP_MO_LOCATION
    GPS_ASSIST_DATA
    GPS_RELEASE_GPS
    GPS_MEASURE_POSITION
    GPS_MTLR_NOTIFICATON
    GPS_RESET_ASSIST_DATA
    GPS_FREQUENCY_AIDING
    GANSS_ASSIST_DATA
    IPC_GPS_CONTROL_PLANE
    GANSS_MEASURE_POSITION
    IPC_FACTORY_WARRANTY_BIT

    RFS responses: id and command same as id and command of request.

    RFS_NV_READ_ITEM                1   reads from the NV data file?
        req:
            4 bytesLE >> #offset,
            4 bytesLE >> #length
        resp:
            1 byte >> #confirm "1 for success, 0 for failure",
            4 bytesLE >> #offset "copied from request",
            4 bytesLE >> #length "copied from request",
            'length' bytes >> #data
    RFS_NV_WRITE_ITEM               2   writes into the NV data file?
        req:
            4 bytesLE >> #offset,
            4 bytesLE >> #length,
            'length' bytes >> #data
        resp:
            1 byte >> #confirm "1 for success, 0 for failure",
            4 bytesLE >> #offset "copied from request",
            4 bytesLE >> #length "copied from request",
    RFS_READ_FILE                   3 ??
    RFS_WRITE_FILE                  4 ??
    RFS_LSEEK_FILE                  5 ??
    RFS_CLOSE_FILE                  6   close(2)
        req:
            4 bytesLE >> #fd
        resp:
            4 bytesLE signed >> #result,
            4 bytesLE signed >> #errno
    RFS_PUT_FILE                    7 ??
    RFS_GET_FILE                    8 ??
    RFS_RENAME_FILE                 9 ??
    RFS_GET_FILE_INFO               10 ??
    RFS_UNLINK_FILE                 11 ??
    RFS_MAKE_DIR                    12 ??
    RFS_REMOVE_DIR                  13 ??
    RFS_OPEN_DIR                    14 ??
    RFS_READ_DIR                    15 ??
    RFS_CLOSE_DIR                   16 ??
    RFS_OPEN_FILE                   17  open(2)
        [0 0 0 0 18 0 0 0 67 80 95 65 85 68 73 79 95 83 76 83 73 46 98 105 110 0]
         4 bytes "flags. O_RDONLY = 0 on linux, so maybe error out if nonzero here"
                 4 bytesLE "length of file path, incl trailing NUL byte"
                          file path
        resp:
            4 bytesLE signed >> #result,
            4 bytesLE signed >> #errno
    RFS_FTRUNCATE_FILE              18 ??
    RFS_GET_HANDLE_INFO             19 ??
    RFS_CREATE_FILE                 20 ??
    RFS_NV_WRITE_ALL_ITEM           21 ??
    RFS_NV_BUFFER_MESSAGE           22 ??
    RFS_NV_RESTORE                  23 ??

    MMS_PROVISION_GET_ITEM_DATA
    MMS_SUB_CMD_UNDEFINED(0x%x)

    IMS_CHANNEL
    IMS_CODEC
    IMS_RTP_MEDIA
    IMS_DTMF
    IMS_OPTION
    IMS_PDN
    IMS_SESSION_REFRESH
    IMS_REGI
    IMS_ENGINE
    IMS_FRAME_TIME
    IMS_DEDICATED_BEARER_INFO
    IMS_INFORMATION
    IMS_TIMER
    IMS_RRC_CONNECTION
    IPC_IMS_CP_STATE
    IPC_IMS_HVOLTE_SWITCH
    IMS_SIP_INFO_ACB
    IMS_SUB_CMD_UNDEFINED(0x%x)

    IPC_DOMESTIC_INVITE_FLUSH
    DOMESTIC_CHANNEL_SETTING_LTE
    DOMESTIC_LTE_CA
    DOMESTIC_PROTOCOL_ERROR_DETECTION
    DOMESTIC_LTE_ROAMING_STATUS
    DOMESTIC_PTT_USIM_LOCK

    QMI_HIDDENMENU_CDMA_DATA_BYTE_COUNTER
    QMI_HIDDENMENU_CDMA_DATA_MOBILE_IP_NAI
    QMI_HIDDENMENU_CDMA_DATA_MIP_NAI_CHANGED
    QMI_HIDDENMENU_CDMA_DATA_MIP_CONNECT_STATUS
    QMI_HIDDENMENU_CDMA_DATA_DDTMMODE_CONFIG
    QMI_HIDDENMENU_CDMA_DATA_WORKING_MODE
    QMI_HIDDENMENU_CDMA_DATA_EVDO_STATE_AND_CONN_ATTEMPT
    QMI_HIDDENMENU_CDMA_CALL_TIME_COUNT
    QMI_HIDDENMENU_CDMA_MODEM_RESET
    QMI_HIDDENMENU_WB_AMR_RPT
    QMI_HIDDENMENU_CDMA_CHANNEL_IO
    QMI_HIDDENMENU_CDMA_BAND_CLASS
    QMI_HIDDENMENU_EHRPD_CONFIG
    QMI_HIDDENMENU_CDMA_DATA_EVDO_AUTH_VALUE
    QMI_HIDDENMENU_BAND26_ENABLED
    QMI_HIDDENMENU_BAND41_ENABLED
    QMI_HIDDENMENU_BAND25_PRIORITY
    QMI_HIDDENMENU_BAND_PROVISIONED
    QMI_HIDDENMENU_BAND25_ENABLED
    QMI_HIDDENMENU_BAND_ENABLED
    QMI_HIDDENMENU_BAND41_TX_SWITCHING_DIVERSITY
    QMI_HIDDENMENU_LTE_ROAMING_ENABLED
    QMI_HIDDENMENU_CA_ENABLED
    QMI_HIDDENMENU_BAND_PRIORITY
    QMI_HIDDENMENU_CA_CONFIG
    IPC_QMI_HIDDENMENU_DATA_IMSIP_INTERFACE_ID
    QMI_HIDDENMENU_CMD_UNDEFINED(0x%x)

    PROSE_CORE_CONTROL
    PROSE_APP_REGIST
    PROSE_APP_SERVER_PROVISION_UPDATE
    PROSE_DISCOVERY_CONTROL
    PROSE_DISCOVERY_STATE
    PROSE_DISCOVERY_QUERY
    PROSE_COMMUNICATION_CONTROL
    PROSE_UE2NETWORK_RELAY_CONTROL
    PROSE_EMBMS_RELAY_CONTROL
    PROSE_CELLID_ANNOUNCEMENT_CONTROL
    PROSE_PER_PACKET_PRIORITY_CONTROL
    PROSE_GEOGRAPHICAL_AREA_INFO
    PROSE_CONFIGURATION_DATA_CONTROL
    PROSE_SIGNALING_CONTROL
    PROSE_UE2NET_RELAY_AVAILABILITY
    PROSE_SYSTEM_TIME_INFO
    PROSE_USER_INFO
    PROSE_SIDELINK_SYNC_INFO
    PROSE_SUB_CMD_UNDEFINED(0x%x)

    MCPTT_FCP_USER_INFO
    MCPTT_PDN_MSG
    MCPTT_FCP_FLOOR_CHANNEL_MSG
    MCPTT_FCP_FLOOR_INFO
    MCPTT_FCP_FLOOR_OPERATION
    MCPTT_FCP_FLOOR_MESSAGE
    MCPTT_FCP_FLOOR_EVENT
    MCPTT_FCP_GNRL_MBMS_SUBCH
    MCPTT_FCP_FLOOR_MBMS_SUBCH
    MCPTT_FCP_SESSION_CALL_CNTRL
    MCPTT_FCP_SECURITY_INFO
    MCPTT_CCP_CHANNEL_MSG
    MCPTT_CCP_PERIODIC_ANNOUNCE_MSG
    MCPTT_CCP_INSTANT_TRANSMISSION_MSG
    MCPTT_MCP_CHANNEL
    MCPTT_MCP_CODEC
    MCPTT_MCP_CONTROL
    MCPTT_MCP_DTMF
    MCPTT_MCP_OPTION
    MCPTT_MCP_FRAME_TIME
    MCPTT_MCP_DEDICATE_BEARER_INFO
    MCPTT_MCP_INFORMATION
    MCPTT_MCP_SIP_INFO_ACB
    MCPTT_SUB_CMD_UNDEFINED(0x%x)

    QMIIMS_DEDICATED_BEARER_INFO
    QMIIMS_ECM_SEARCH
    QMIIMS_ECM_FOUND_SVC
    QMIIMS_SUB_CMD_UNDEFINED(0x%x)







SMS examples

[pid  4916] write(18</dev/umts_ipc0>, "+\0`\0\4\1\1\2\2\0 \7\221\23V\23\23\23\363\1\0\v\221\23V\227HC\367\0\0\f\324\362\234\16\242\313\303\343\264\373\f", 43) = 43 <0.000118>
[pid  4918] read(18</dev/umts_ipc0>, "\f\0\214\0\4\1\3\2\0\0\2\0", 264192) = 12 <0.000242>

[pid  4918] read(18</dev/umts_ipc0>, "/\0\221\0\4\2\3\2\1\377\377\1\"\7\221\23V\23\23\23\363\4\v\221\23V\227HC\367\0\0\2\220p\2\203\220\200\10A\220\274\fg\347C", 264192) = 47 <0.000217>
[pid  4916] write(18</dev/umts_ipc0>, "\f\0a\0\4\6\1\2\0\0\1\0", 12) = 12 <0.000157>
[pid  4918] read(18</dev/umts_ipc0>, "\f\0\222\0\200\1\2\4\6\1\0\200", 264192) = 12 <0.000087>

2020-09-16 22:54:20 (87200) a SamsungFmtMessage(40:0 #SMS/#'SMS_INCOMING_MSG'/#noti #[2 1 255 255 1 38 7 145 19 86 19 19 19 243 0 11 145 19 86 151 72 67 247 0 0 2 144 49 50 18 99 128 12 212 247 155 204 46 131 230 227 247 155 14] '.....&...V........V.HC.....12.c.............')
2020-09-16 23:08:59 (LinuxInputTestSink) Generic: a SamsungFmtMessage(34:0 #SMS/#'SMS_INCOMING_MSG'/#noti #[2 1 255 255 1 38 7 145 19 86 19 19 19 243 0 11 145 19 86 151 72 67 247 0 0 2 144 49 50 18 99 128 12 212 247 155 204 46 131 230 227 247 155 14] '.....&...V........V.HC.....12.c.............')
2020-09-16 23:08:59 (LinuxInputTestSink) SMS: SMS_INCOMING_MSG#[2 1 255 255 1 38 7 145 19 86 19 19 19 243 0 11 145 19 86 151 72 67 247 0 0 2 144 49 50 18 99 128 12 212 247 155 204 46 131 230 227 247 155 14]'.....&...V........V.HC.....12.c.............'




Call examples - INCOMING call

						[G] RX: (M)CALL_CMD (S)CALL_INCOMING (T)NOTI l:b m:d3 a:00 [ 00 01 00 01 ]
						[G] RX: (M)SS_CMD (S)SS_INFO (T)NOTI l:81 m:d4 a:00 [ 31 00 00 FF 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ]
						[G] RX: (M)SND_CMD (S)SND_WB_AMR_RPT (T)NOTI l:8 m:d5 a:00 [ 01 ]
						[G] RX: (M)SND_CMD (S)SND_CLOCK_CTRL (T)NOTI l:8 m:d6 a:00 [ 05 ]

			[G] TX: (M)CALL_CMD (S)CALL_LIST (T)GET l:7 m:80 a:00 [ ]
						[G] RX: (M)CALL_CMD (S)CALL_LIST (T)RESP l:1c m:d7 a:80 [ 01 00 01 00 02 05 00 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37 ]

						[G] RX: (M)DISP_CMD (S)DISP_RSSI_INFO (T)NOTI l:13 m:d8 a:00 [ 69 04 63 63 00 FF FF FF FF 12 FD FF ]

			[G] TX: (M)SND_CMD (S)SND_MIC_MUTE_CTRL (T)SET l:8 m:81 a:00 [ 00 ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:d9 a:81 [ 09 02 03 00 80 ]

			[G] TX: (M)CALL_CMD (S)CALL_ANSWER (T)EXEC l:7 m:82 a:00 [ ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:da a:82 [ 02 04 01 00 80 ]

			[G] TX: (M)SND_CMD (S)SND_AUDIO_PATH_CTRL (T)SET l:9 m:83 a:00 [ 01 00 ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:db a:83 [ 09 03 03 00 80 ]

						[G] RX: (M)SND_CMD (S)SND_CLOCK_CTRL (T)NOTI l:8 m:dc a:00 [ 05 ]
						[G] RX: (M)SND_CMD (S)SND_RINGBACK_TONE_CTRL (T)NOTI l:8 m:dd a:00 [ 00 ]
						[G] RX: (M)CALL_CMD (S)CALL_STATUS (T)NOTI l:d m:de a:00 [ 00 01 00 03 00 00 ]

			[G] TX: (M)CALL_CMD (S)CALL_LIST (T)GET l:7 m:84 a:00 [ ]
						[G] RX: (M)CALL_CMD (S)CALL_LIST (T)RESP l:1c m:df a:84 [ 01 00 01 00 02 01 00 0C 11 2B 33 31 36 35 37 39 38 34 33 34 37 ]

			[G] TX: (M)SND_CMD (S)SND_TWO_MIC_SOL_CTRL (T)SET l:9 m:85 a:00 [ 00 01 ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:e0 a:85 [ 09 0B 03 00 80 ]

			[G] TX: (M)SND_CMD (S)SND_SPKR_VOLUME_CTRL (T)SET l:9 m:86 a:00 [ 01 04 ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:e1 a:86 [ 09 01 03 00 80 ]
						[G] RX: (M)MISC_CMD (S)MISC_TIME_INFO (T)NOTI l:19 m:e2 a:00 [ 02 01 14 09 08 0B 19 38 08 01 02 01 32 30 34 30 38 23 ]
						[G] RX: (M)DISP_CMD (S)DISP_RSSI_INFO (T)NOTI l:13 m:e3 a:00 [ 65 06 63 63 00 FF FF FF FF 11 FD FF ]

			[G] TX: (M)CALL_CMD (S)CALL_RELEASE (T)EXEC l:7 m:87 a:00 [ ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:e4 a:87 [ 02 03 01 00 80 ]

						[G] RX: (M)SND_CMD (S)SND_RINGBACK_TONE_CTRL (T)NOTI l:8 m:e5 a:00 [ 00 ]
						[G] RX: (M)CALL_CMD (S)CALL_STATUS (T)NOTI l:d m:e6 a:00 [ 00 01 00 04 00 05 ]
						[G] RX: (M)SVC_CMD (S)SVC_BIG_DATA_INFO (T)NOTI l:e5 m:e7 a:00 [ 00 DB 00 7B 22 4A 56 45 52 22 3A 22 53 50 4C 31 22 2C 22 48 57 5F 56 22 3A 22 4D 50 5F 30 2E 37 30 30 22 2C 22 43 74 79 70 22 3A 22 31 22 2C 22 50 4C 4D 4E 22 3A 22 32 30 34 30 38 23 22 2C 22 41 43 54 5F 22 3A 22 32 22 2C 22 52 41 43 5F 22 3A 22 30 22 2C 22 4C 41 43 5F 22 3A 22 31 39 35 30 22 2C 22 54 41 43 5F 22 3A 22 30 30 30 30 22 2C 22 43 5F 49 44 22 3A 22 41 37 32 31 46 30 22 2C 22 50 68 49 44 22 3A 22 33 37 38 22 2C 22 44 4C 43 68 22 3A 22 33 30 31 31 22 2C 22 52 67 53 74 22 3A 22 32 22 2C 22 52 6A 43 75 22 3A 22 30 22 2C 22 47 52 49 50 22 3A 22 32 22 2C 22 45 41 52 4A 22 3A 22 30 22 2C 22 41 55 53 54 22 3A 22 34 22 2C 22 54 78 41 53 22 3A 22 33 22 7D ]

			[G] TX: (M)CALL_CMD (S)CALL_LIST (T)GET l:7 m:88 a:00 [ ]
						[G] RX: (M)CALL_CMD (S)CALL_LIST (T)RESP l:8 m:e8 a:88 [ 00 ]

			[G] TX: (M)SND_CMD (S)SND_AUDIO_PATH_CTRL (T)SET l:9 m:89 a:00 [ 01 00 ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:e9 a:89 [ 09 03 03 00 80 ]

			[G] TX: (M)SND_CMD (S)SND_CLOCK_CTRL (T)EXEC l:8 m:8a a:00 [ 00 ]
						[G] RX: (M)GEN_CMD (S)GEN_PHONE_RES (T)RESP l:c m:ea a:8a [ 09 09 01 00 80 ]

						[G] RX: (M)SND_CMD (S)SND_CLOCK_CTRL (T)NOTI l:8 m:eb a:00 [ 00 ]

