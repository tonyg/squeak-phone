#!/usr/bin/env python3

import sys
import re

def unescape1(m):
    v = int(m[1], 16)
    return m[0] if v < 32 or v > 127 else chr(v)

for line in sys.stdin.readlines():
    sys.stdout.write(re.sub(r'\\x([0-9a-fA-F]{2})', unescape1, line))
