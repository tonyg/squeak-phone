## Anatomy of an incoming call

Regular call strength indicators, but immediately prior to
registration changing (just before the call -- did it move me to a new
cell for the call because it was a better fit for the traffic that was
about to start?):

    09:42:34.644010+01:00 [HayesModem]: +QIND: "csq",99,99

Interestingly the first 99 there means "not known or not detectable",
maybe because we're changing cell and it doesn't know what the signal
strength will be like there yet.

Here we change cell:

    09:42:34.644037+01:00 [HayesModem]: +CREG: 1,"1950","0ADC20C",2
    09:42:34.644046+01:00 [HayesModem]: +CGREG: 1,"1950","0ADC20C",2

Oh! A call! The CLIP's final 0 tells us the CLI is "valid". The DSCI
tells us call #2 is MT (1), incoming (4), voice (0), from my main
phone, which is in international format (145):

    09:42:34.889393+01:00 [HayesModem]: +CRING: VOICE
    09:42:34.889410+01:00 [HayesModem]: +CLIP: "+31612345678",145,,,,0
    09:42:34.889418+01:00 [HayesModem]: ^DSCI: 2,1,4,0,+31612345678,145

We learn the signal strength at the new cell:

    09:42:38.160627+01:00 [HayesModem]: +QIND: "csq",12,99

We get a time signal from the network:

    09:42:40.069934+01:00 [HayesModem]: +CTZE: "+04",0,"2021/12/16,08:42:39"

Another ring:

    09:42:40.893283+01:00 [HayesModem]: +CRING: VOICE
    09:42:40.893309+01:00 [HayesModem]: +CLIP: "+31612345678",145,,,,0

Another:

    09:42:47.003871+01:00 [HayesModem]: +CRING: VOICE
    09:42:47.003906+01:00 [HayesModem]: +CLIP: "+31612345678",145,,,,0

I hung up on the other end. DSCI says call #2, MT, state CALL_END (6), voice (0).

    09:42:47.354649+01:00 [HayesModem]: ^DSCI: 2,1,6,0,+31612345678,145
    09:42:47.365856+01:00 [HayesModem]: NO CARRIER

    09:42:49.361249+01:00 [HayesModem]: +QIND: "csq",20,99

Back (?) to the old cell (?):

    09:42:49.368473+01:00 [HayesModem]: +CREG: 1,"FD21","12B1B20",7
    09:42:49.368493+01:00 [HayesModem]: +CGREG: 1,"FD21","12B1B20",7

And back to regular signal strength indicators:

    09:42:50.151209+01:00 [HayesModem]: +QIND: "csq",14,99
    09:42:52.186241+01:00 [HayesModem]: +QIND: "csq",19,99
    09:43:01.303893+01:00 [HayesModem]: +QIND: "csq",16,99
    09:43:03.586971+01:00 [HayesModem]: +QIND: "csq",20,99

# Audio

Here's the audio config from the factory:

    +QDAI: 1,1,0,1,0,0,1,1

I'm trying the following on advice from https://xnux.eu/devices/feature/modem-pp.html:

    AT+QDAI=1,0,0,2,0,1,1,1

Oh, that sucks. Horribly noisy output. Going back to the defaults
produces somewhat workable audio:

    AT+QDAI=1,1,0,1,0,0,1,1

Some experiments:

    AT+QDAI=1,1,0,1,0,0,1,1
    ok, has a click (default)

    AT+QDAI=1,0,0,1,0,0,1,1
    noisy, horrible

    AT+QDAI=1,1,0,2,0,1,1,1
    semi-noisy, underwater mic sound on remote end

    AT+QDAI=1,1,0,1,0,1,1,1
    ok, has a click

    AT+QDAI=1,1,0,2,0,0,1,1
    semi-noisy, underwater mic sound on remote end

# Network

https://techship.com/faq/how-to-step-by-step-set-up-a-data-connection-over-qmi-interface-using-qmicli-and-in-kernel-driver-qmi-wwan-in-linux/

    ifconfig wwan0 down
    echo Y | tee /sys/class/net/wwan0/qmi/raw_ip
    ifconfig wwan0 up
    qmicli --device=/dev/cdc-wdm0 --device-open-proxy --wds-start-network=ip-type=4 --client-no-release-cid
    udhcpc -v -q -f -n -i wwan0
