# PinePhone docs

 - `Quectel_EC2x&EG9x&EG2x-G&EM05_Series_AT_Commands_Manual_V2.0.pdf`, from https://wiki.pine64.org/wiki/File:Quectel_EC2x%26EG9x%26EG2x-G%26EM05_Series_AT_Commands_Manual_V2.0.pdf
 - `modem.txt`, from https://megous.com/dl/tmp/modem.txt
 - `Modem on PinePhone.html`, from https://xnux.eu/devices/feature/modem-pp.html
 - `EG25-G reverse engineering.html`, from https://xnux.eu/devices/feature/modem-pp-reveng.html

The modem AT interface is on `/dev/EG25.AT`, which is a symlink (!) to `ttyUSB2`:

    lrwxrwxrwx    1 root     root             7 Nov 26 15:17 EG25.AT -> ttyUSB2
    lrwxrwxrwx    1 root     root             7 Nov 26 15:17 EG25.MODEM -> ttyUSB3
    lrwxrwxrwx    1 root     root             7 Nov 26 15:17 EG25.NMEA -> ttyUSB1

Generally useful settings:

    ATE0Q0V1X4
    AT+CMEE=1

 - E0 = don't echo commands sent to modem
 - Q0 = don't be quiet, output responses
 - V1 = use verbose response codes (alternative is numeric, might be worth looking into)
 - X4 = use detailed CONNECT responses, dial tone detection, and busy tone detection
 - +CMEE=1 = use numeric error codes (cf +CMEE=2, verbose values)

Result codes "OK", "CONNECT", "RING", "NO CARRIER", "ERROR", "NO DIALTONE", "BUSY", "NO ANSWER"

ATS3 and ATS4 allow customised replacements of CR and LF,
respectively; might be useful for improved packetisation??

 - AT+CFUN=0 --> minimum functionality
 - AT+CFUN=1 --> full functionality
 - AT+CFUN=1,1 --> full functionality and RESET THE MODEM
 - AT+CFUN=4 --> transmit and receive RF signals DISABLED (flight mode??)

Consider UCS2 character set selection via `AT+CSCS="UCS2"`.

Error code table (see table 22, section 15.4, page 293 of the Quectel
manual):

| Error code     | Error message                |
|----------------|------------------------------|
| +CME ERROR: 10 | +CME ERROR: SIM not inserted |
| +CME ERROR: 13 | +CME ERROR: SIM failure      |

URC = Unsolicited Response Code. You can tell it to send URCs to a
different port, to avoid interference with the regular AT
command/response parsing. See command `AT+QURCCFG`.

Interesting stuff about +QCFG configuring individual URC types here:
https://todo.sr.ht/~anteater/mms-stack-bugs/40

Most stacks seem to set URCs to go to the AT interface ("usbat"). The
modem's default seems to be "all".

Lots of great stuff here
https://gitlab.com/mobian1/devices/eg25-manager (of course!)

 - AT+CPAS gets call status: 0 ready, 3 ringing, 4 call in progress or call on hold
 - AT+CEER gets extended error report
