#lang racket

(require plot)
(require 2htdp/batch-io)

(define percentages
  (read-csv-file/rows "battery-rundown-log.csv"
                      (lambda (row) (string->number (second row)))))

(plot (lines (for/list [(p percentages)
                        (i (in-naturals))]
               (vector (/ i 60.0) p))
             ;#:label "PinePhone battery discharge curve"
             )
      #:width 640
      #:title "PinePhone battery discharge curve"
      #:x-label "Hours elapsed"
      #:y-label "Percentage capacity remaining"
      )